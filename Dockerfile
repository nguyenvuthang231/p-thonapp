# This file is a template, and might need editing before it works on your project.
FROM python:3.4-alpine
ADD . /code
WORKDIR /code

RUN pip install   -r requirements.txt

CMD ["python", "app.py"]

